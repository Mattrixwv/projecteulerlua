--ProjectEuler/lua/Problem6.lua
--Matthew Ellison
-- Created: 02-06-19
--Modified: 06-19-20
--Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum
--All of my requires, unless otherwise listed, can be found at https://bitbucket.org/Mattrixwv/luaClasses
--[[
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]


require "Stopwatch"


local timer = Stopwatch:create();
timer:start();

--Create the variables you need
local sumOfSquares = 0;
local squareOfSum = 0;
--Loop through all numbers 1-100 to do the appropriate math with them
for num=1,100 do
	sumOfSquares = sumOfSquares + num^2;	--Get the sum of the squares of the first 100 natural number
	squareOfSum = squareOfSum + num;	--Get the sum of the first 100 natural numbers
end
--Square the normal sum
squareOfSum = squareOfSum^2;

timer:stop();

--Print the result
print("The difference between the sum of the squares and the square of the sum of the numbers 1-100 is " .. math.floor(math.abs(sumOfSquares - squareOfSum)));
print("It took " .. timer:getMicroseconds() .. " microseconds to run this algorithm");

--[[Results:
The difference between the sum of the squares and the square of the sum of the numbers 1-100 is 25164150
It took 53 microseconds to run this algorithm
]]
