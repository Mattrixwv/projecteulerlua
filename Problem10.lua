--ProjectEuler/lua/Problem10.lua
--Matthew Ellison
-- Created: 02-06-19
--Modified: 06-19-20
--Find the sum of all the primes below two million
--All of my requires, unless otherwise listed, can be found at https://bitbucket.org/Mattrixwv/luaClasses
--[[
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]


require "Stopwatch"
require "Algorithms"


local timer = Stopwatch:create();
timer:start();

LARGEST_PRIME = 2000000;	--Sum all prime numbers smaller than this

--Get all of the prime numbers < LARGEST_PRIME
local primes = getPrimes(LARGEST_PRIME);

--Get the sum of the table
local sumOfPrimes = 0;
for location=1,#primes do
	sumOfPrimes = sumOfPrimes + primes[location];
end

timer:stop();

--Print the results
print("The sum of all prime numbers less than " .. LARGEST_PRIME .. " is " .. sumOfPrimes);
print("It took " .. timer:getSeconds() .. " seconds to run this algorithm");

--[[Results:
The sum of all prime numbers less than 2000000 is 142913828922
It took 5.940409 seconds to run this algorithm
]]
