--ProjectEuler/lua/Problem27.lua
--Matthew Ellison
-- Created: 09-15-19
--Modified: 06-19-20
--Find the product of the coefficients, |a| < 1000 and |b| <= 1000, for the quadratic expression that produces the maximum number of primes for consecutive values of n, starting with n=0.
--All of my requires, unless otherwise listed, can be found at https://bitbucket.org/Mattrixwv/luaClasses
--[[
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]


require "Stopwatch"
require "Algorithms"


--Setup the variables
local timer = Stopwatch:create();
local topA = 0; --The A for the most n's generated
local topB = 0; --The B for the most n's generated
local topN = 0; --The most n's generated
local primes = getPrimes(12000) --A list of all primes that could possibly be generated with this formula


--Start the timer
timer:start();

--Start with the lowest possible A and check all possibilities after that
for a = -999, 999 do
	--Start with the lowest possible B and check all possibilities after that
	for b = -1000, 1000 do
		--Start with n=0 and check the formula to see how many primes you can get get with concecutive n's
		local n = 0;
		local quadratic = (n * n) + (a * n) + b;
		while(isFound(primes, quadratic)) do
			n = n + 1;
			quadratic = (n * n) + (a * n) + b;
		end
		n = n - 1;  --Negate an n because the last formula failed

		--Set all the largest numbers if this created more primes than any other
		if(n > topN) then
			topN = n;
			topB = b;
			topA = a;
		end
	end
end


--Stop the timer
timer:stop();

--Print the results
io.write("The greatest number of primes found is " .. topN .. '\n');
io.write("It was found with A = " .. topA .. ", B = " .. topB .. '\n');
io.write("The product of A and B is " .. topA * topB .. '\n');
io.write("It took " .. timer:getString() .. " to run this algorithm\n");

--[[ Results:
The greatest number of primes found is 70
It was found with A = -61, B = 971
The product of A and B is -59231
It took 119.350 seconds to run this algorithm
]]
