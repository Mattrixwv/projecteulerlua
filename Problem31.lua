--ProjectEuler/lua/Problem31.lua
-- Created: 06-19-20
--Modified: 06-19-20
--How many different ways can £2 be made using any number of coins?
--All of my requires, unless otherwise listed, can be found at https://bitbucket.org/Mattrixwv/luaClasses
--[[
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]


require "Stopwatch"


--Setup the variables
local timer = Stopwatch:create();
local desiredValue = 200;
local permutations = 0;

--Start the timer
timer:start();

--Start with 200p and remove the necessary coins with each loop
local pound2 = desiredValue;
while(pound2 >= 0) do
	local pound1 = pound2;
	while(pound1 >= 0) do
		local pence50 = pound1;
		while(pence50 >= 0) do
			local pence20 = pence50;
			while(pence20 >= 0) do
				local pence10 = pence20;
				while(pence10 >= 0) do
					local pence5 = pence10;
					while(pence5 >= 0) do
						local pence2 = pence5;
						while(pence2 >= 0) do
							permutations = permutations + 1;
							pence2 = pence2 - 2;
						end
						pence5 = pence5 - 5;
					end
					pence10 = pence10 - 10;
				end
				pence20 = pence20 - 20;
			end
			pence50 = pence50 - 50;
		end
		pound1 = pound1 - 100;
	end
	pound2 = pound2 - 200;
end

--Stop the timer
timer:stop();

--Print the results
io.write("There are " .. permutations .. " ways to make 2 pounds with the given denominations of coins\n");
io.write("It took " .. timer:getString() .. " to run this algorithm\n");


--[[ Results:
There are 73682 ways to make 2 pounds with the given denominations of coins
It took 1.000 milliseconds to run this algorithm
]]
