--ProjectEuler/ProjectEulerLua/Problem37.lua
--Matthew Ellison
-- Created: 07-01-21
--Modified: 07-01-21
--Find the sum of the only eleven primes that are both truncatable from left to right and right to left (2, 3, 5, and 7 are not counted).
--All of my requires, unless otherwise listed, can be found at https://bitbucket.org/Mattrixwv/luaClasses
--[[
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]


require "Stopwatch"
require "Algorithms"
require "SieveOfEratosthenes"


--Setup the variables
local timer = Stopwatch:create();
local LAST_PRIME_BEFORE_CHECK = 7;	--The last prime before 11 since single digit primes aren't checked
local truncPrimes = {};	--All numbers that are truncatable primes
local sum = 0;	--The sum of all elements in truncPrimes

--Start the timer
timer:start();

--Create the sieve and get the first prime number
local sieve = SieveOfEratosthenes:create();
local currentPrime = sieve:next();
--Loop through the sieve until you get to LAS_PRIME_BEFORE_CHECK
while(currentPrime < LAST_PRIME_BEFORE_CHECK) do
	currentPrime = sieve:next();
end
--Loop until truncPrimes contains 11 elements
while(#truncPrimes < 11)do
	local isTruncPrime = true;
	--Get the next rpime
	currentPrime = sieve:next();
	--Convert the prime to a string
	local primeString = tostring(currentPrime);
	--If the string contains an even digit move to the next prime
	local strLoc = 1;
	while((strLoc <= #primeString) and (isTruncPrime)) do
		local char = string.sub(primeString, strLoc, strLoc);
		--Allow 2 to be the first digit
		if((strLoc == 1) and (char == "2")) then
		else
			if((char == "0") or (char == "2") or (char == "4") or (char == "6") or (char == "8")) then
				isTruncPrime = false;
			end
		end
		strLoc = strLoc + 1;
	end
	--Start removing digits from the left and see if the number stays prime
	if(isTruncPrime) then
		for truncLoc = 2, #primeString do
			--Create a substring of the prime, removing the needed digits from the left
			local primeSubstring = string.sub(primeString, truncLoc);
			--Convert the string to an int and see if the number is still prime
			local newPrime = tonumber(primeSubstring);
			if(not isPrime(newPrime)) then
				isTruncPrime = false;
				break;
			end
		end
	end
	--Start removing digits from the right and see if the number stays prime
	if(isTruncPrime) then
		for truncLoc = 1, #primeString - 1 do
			--Create a substring of the prime, removing the needed digits from the right
			local primeSubstring = string.sub(primeString, 1, #primeString - truncLoc);
			--Convert the string to an int and see if the number is still a prime
			local newPrime = tonumber(primeSubstring);
			if(not isPrime(newPrime)) then
				isTruncPrime = false;
				break;
			end
		end
	end
	--If the number remained prime through all operations add it to the table
	if(isTruncPrime) then
		table.insert(truncPrimes, currentPrime);
	end
end
--Get the sum of all elements in the truncPrimes vector
sum = getSum(truncPrimes);

--Stop the timer
timer:stop();

--Print the results
io.write("The sum of all left and right truncatable primes is " .. sum .. "\n");
io.write("It took " .. timer:getString() .. " to run this algorithm\n");


--[[ Results:
The sum of all left and right truncatable primes is 748317
It took 491.000 milliseconds to run this algorithm
]]
