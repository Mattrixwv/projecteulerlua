--ProjectEuler/lua/Problem18.lua
--Matthew Ellison
-- Created: 03-12-19
--Modified: 06-19-20
--Find the maximum total from top to bottom
--[[
75
95 64
17 47 82
18 35 87 10
20 04 82 47 65
19 01 23 75 03 34
88 02 77 73 07 63 67
99 65 04 28 06 16 70 92
41 41 26 56 83 40 80 70 33
41 48 72 33 47 32 37 16 94 29
53 71 44 65 25 43 91 52 97 51 14
70 11 33 28 77 73 17 78 39 68 17 57
91 71 52 38 17 14 91 43 58 50 27 29 48
63 66 04 68 89 53 67 30 73 16 69 87 40 31
04 62 98 27 23 09 70 98 73 93 38 53 60 04 23
]]
--This is done using a breadth first search
--All of my requires, unless otherwise listed, can be found at https://bitbucket.org/Mattrixwv/luaClasses
--[[
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]


require "Stopwatch"

--[[ This is what locations should look like
location = {
	xLocation = 0,
	yLocation = 0,
	total = 0,
	fromRight = false
};
]]

local function invert(list)
	for rowCnt=1,NUM_ROWS do
		for colCnt=1,#list[rowCnt] do
			list[rowCnt][colCnt] = 100 - list[rowCnt][colCnt];
		end
	end
end

local function foundInList(list, loc)
	for location=1,#list do
		if((list[location].xLocation == loc.xLocation) and (list[location].yLocation == loc.yLocation)) then
			return true;
		end
	end
	return false;
end

local function remove_if(list, loc)
	local location = 1;
	while(location <= #list) do
		if((list[location].xLocation == loc.xLocation) and (list[location].yLocation == loc.yLocation)) then
			table.remove(list, location);
		else
			location = location + 1;
		end
	end
end

--Create a timer and time the algorithm
local timer = Stopwatch:create();
timer:start();

local NUM_ROWS = 15;


local list = {};
list[1]  = {75};
list[2]  = {95, 64};
list[3]  = {17, 47, 82};
list[4]  = {18, 35, 87, 10};
list[5]  = {20, 04, 82, 47, 65};
list[6]  = {19, 01, 23, 75, 03, 34};
list[7]  = {88, 02, 77, 73, 07, 63, 67};
list[8]  = {99, 65, 04, 28, 06, 16, 70, 92};
list[9]  = {41, 41, 26, 56, 83, 40, 80, 70, 33};
list[10] = {41, 48, 72, 33, 47, 32, 37, 16, 94, 29};
list[11] = {53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14};
list[12] = {70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57};
list[13] = {91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48};
list[14] = {63, 66, 04, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31};
list[15] = {04, 62, 98, 27, 23, 9, 70, 98, 73, 93, 38, 53, 60, 04, 23};

--Invert the list so all elements are 100 - element
invert(list);

local foundPoints = {};	--This is a table of locations
foundPoints[1] = {xLocation = 1, yLocation = 1, total = list[1][1], fromRight = false};
local possiblePoints = {};	--This is a table of locations
--Add the second row as possible points
possiblePoints[1] = {xLocation = 1, yLocation = 2, total = (list[1][1] + list[2][1]), fromRight = true};
possiblePoints[2] = {xLocation = 2, yLocation = 2, total = (list[1][1] + list[2][2]), fromRight = false};
local foundBottom = false;	--Used when you find a point at the bottom

--Loop until you find the bottom
while(not foundBottom) do
	--Check which possible point gives us the lowest number. If more than one has the same number simply keep the first one
	local minLoc = possiblePoints[1];
	for loc=1,#possiblePoints do
		if(possiblePoints[loc].total < minLoc.total) then
			minLoc = possiblePoints[loc];
		end
	end

	--Remove it from the list of possible points
	remove_if(possiblePoints, minLoc);

	--[[ Aids in tracing the trail instead of using line after this
	--Add that point to the list of found points
	if(not foundInList(foundPoints, minLoc)) then
		table.insert(foundPoints, minLoc);
	end
	]]
	table.insert(foundPoints, minLoc);

	--Add to the list of possible points from the point we just found and
	--If you are at the bottom raise the flag to end the program
	local xLoc = minLoc.xLocation;
	local yLoc = minLoc.yLocation + 1;	--Add one because you will always be moving to the next row
	if(yLoc > NUM_ROWS) then
		foundBottom = true;
	else
		--print("minLoc.total = " .. minLoc.total);
		--print("list[" .. yLoc .. "][" .. xLoc .. "] = ");
		table.insert(possiblePoints, {xLocation = xLoc, yLocation = yLoc, total = (minLoc.total + list[yLoc][xLoc]), fromRight = true});
		xLoc = xLoc + 1;	--Advance the x location to simulate going right
		table.insert(possiblePoints, {xLocation = xLoc, yLocation = yLoc, total = (minLoc.total + list[yLoc][xLoc]), fromRight = false});
	end
end

local actualTotal = ((100 * (NUM_ROWS)) - foundPoints[#foundPoints].total);

--Stop the timer
timer:stop();

--Reinvert the list so it will print propperly
invert(list);

--Print the results
print("The value of the longest path is " .. actualTotal);
print("It took " .. timer:getMilliseconds() .. " milliseconds to run this algorithm");

--[[
--Print the pyramid of numbers out
for rowCnt=1,NUM_ROWS do
	for colCnt=1,#list[rowCnt] do
		if(list[rowCnt][colCnt] < 10) then
			io.write(' ' .. list[rowCnt][colCnt] .. ' ');
		else
			io.write(list[rowCnt][colCnt] .. ' ');
		end
	end
	print("");
end
print("\n\n")

--Find the trail followed
trail = {};	--Holds the locations that were traveled along
table.insert(trail, 1, foundPoints[#foundPoints]);	--Start with the last point and continue adding to the front of the list
top = false;
while(not top) do
	--Find the shortest way from the last location to the current one
	found = false;
	location = #foundPoints;
	while(not found) do
		--print("Location: " .. location);
		--print("trail[1].xLocation = " .. trail[1].xLocation .. "\ntrail[1].yLocation = " .. trail[1].yLocation);
		--print("foundPoints[location].xLocation = " .. foundPoints[location].xLocation .. "\nfoundPoints[location].yLocation = " .. foundPoints[location].yLocation);
		if(trail[1].fromRight) then
			if((foundPoints[location].xLocation == trail[1].xLocation) and (foundPoints[location].yLocation == (trail[1].yLocation - 1))) then
				found = true;
			else
				location = location - 1;
			end
		else
			if((foundPoints[location].xLocation == (trail[1].xLocation) - 1) and (foundPoints[location].yLocation == (trail[1].yLocation - 1))) then
				found = true;
			else
				location = location - 1;
			end
		end
	end

	--Insert the location into the trail
	table.insert(trail, 1, foundPoints[location]);

	--If the current location is 1 then we are at the top
	if(trail[1].yLocation == 1) then
		top = true;
	end
end


--Print the trail
for location=1,#trail do
	io.write(list[trail[location].yLocation][trail[location].xLocation] .. "->");
end
print("");
]]


--[[ Results:
The value of the longest path is 1074
It took 1.162 milliseconds to run this algorithm
]]
