--ProjectEuler/lua/Problem5.lua
--Matthew Ellison
-- Created: 02-05-19
--Modified: 06-19-20
--What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
--All of my requires, unless otherwise listed, can be found at https://bitbucket.org/Mattrixwv/luaClasses
--[[
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]


require "Stopwatch"


local timer = Stopwatch:create();
timer:start();

--Setup the variables you need
local numFound = false;
local currentNum = 22;	--Start looking at 22 becuase it must be divisible by 2 and greater than 20
--Start a loop looking for the correct number
while((not numFound) and (currentNum > 0)) do
	--Set that you found the number to true because you set this flag when you don't find it
	numFound = true;
	--See if the current number is divisible by all number from 1 to 20
	for divisor=1,20 do
		--If it is not set a flag to move to the next possible number
		if((currentNum % divisor) ~= 0) then
			numFound = false;
			break;
		end
	end

	--Increment the number by 2 to check the next one if you didn't find the number
	if(not numFound) then
		currentNum = currentNum + 2
	end
end

timer:stop();

--Print the results
if(currentNum < 0) then
	print("There was an error: Could not find a number that fit the criteria");
else
	print("The smallest positive number that is evenly divisible by all numbers 1-20 is " .. currentNum);
	print("It took " .. timer:getSeconds() .. " seconds to run this algorithm")
end

--[[Results:
The smallest positive number that is evenly divisible by all numbers 1-20 is 232792560
It took 24.788324 seconds to run this algorithm
]]
