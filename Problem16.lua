--ProjectEuler/lua/Problem16.lua
--Matthew Ellison
-- Created: 02-07-19
--Modified: 06-19-20
--What is the sum of the digits of the number 2^1000?
--All of my requires, unless otherwise listed, can be found at https://bitbucket.org/Mattrixwv/luaClasses
--I used the bigint library from https://github.com/empyreuma/bigint.lua
--[[
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]


require "Stopwatch"
local bigint = require("bigint")


local timer = Stopwatch:create();
timer:start();

--Get the number
local num = bigint.exponentiate(bigint.new(2), bigint.new(1000));

--Change the number to the string
local stringOfNum = bigint.unserialize(num, 's');

--Step through the string one element at a time
local sumOfNum = 0;
for location=1,string.len(stringOfNum) do
	--Treat the character like a num and add it to the sum
	sumOfNum = sumOfNum + string.sub(stringOfNum, location, location);
end

timer:stop();

--Print the results
print("2^1000 = " .. stringOfNum);
print("The sum of the digits is: " .. math.floor(sumOfNum));
print("It took " .. timer:getMilliseconds() .. " milliseconds to run this algorithm");

--[[Results:
2^1000 = 10715086071862673209484250490600018105614048117055336074437503883703510511249361224931983788156958581275946729175531468251871452856923140435984577574698574803934567774824230985421074605062371141877954182153046474983581941267398767559165543946077062914571196477686542167660429831652624386837205668069376
The sum of the digits is: 1366
It took 352.0 milliseconds to run this algorithm
]]
