--ProjectEuler/lua/Problem7.lua
--Matthew Ellison
-- Created: 02-05-19
--Modified: 06-19-20
--What is the 10001th prime number?
--All of my requires, unless otherwise listed, can be found at https://bitbucket.org/Mattrixwv/luaClasses
--[[
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]


require "Stopwatch"
require "Algorithms"


local timer = Stopwatch:create();
timer:start();

NUM_PRIMES = 10001;	--The number of the prime number desired
--Get the correct number of primes
local primes = getNumPrimes(NUM_PRIMES);

timer:stop();

--Print the results
print("The " .. NUM_PRIMES .. "th prime number is " .. primes[NUM_PRIMES]);
print("It took " .. timer:getMilliseconds() .. " milliseconds to run this algorithm");

--[[Results:
The 10001th prime number is 104743
It took 127.505 milliseconds to run this algorithm
]]
