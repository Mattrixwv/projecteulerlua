--ProjectEuler/lua/Problem3.lua
--Matthew Ellison
-- Created: 02-04-19
--Modified: 06-19-20
--The largest prime factor of 600851475143
--All of my requires, unless otherwise listed, can be found at https://bitbucket.org/Mattrixwv/luaClasses
--[[
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]


require "Stopwatch"
require "Algorithms"


local timer = Stopwatch:create();
timer:start();


TARGET_NUMBER = 600851475143;
--Get the factors of the number
local factors = getFactors(TARGET_NUMBER);
timer:stop();

--The largest number will be the answer
--Print the results
print("The largest prime factor of " .. TARGET_NUMBER .. " is " .. factors[#factors]);
print("It took " .. timer:getSeconds() .. " seconds to run this algorithm");

--[[Results:
The largest prime factor of 600851475143 is 6857
It took 1.612 seconds to run this algorithm
]]
