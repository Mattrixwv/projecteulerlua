--ProjectEuler/lua/Problem26.lua
--Matthew Ellison
-- Created: 08-02-19
--Modified: 06-19-20
--Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.
--All of my requires, unless otherwise listed, can be found at https://bitbucket.org/Mattrixwv/luaClasses
--[[
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]


require "Stopwatch"

local TOP_NUMBER = 999	--The largest denominator to tbe checked


--Setup the variables
local timer = Stopwatch:create();
local longestCycle = 0;
local longestNumber = 1;

--Start the timer
timer:start();

--Start with 1/2 and find out how long the longest cycle is by checking the remainders
--Loop through every number from 2-999 and use it for the denominator
for denominator = 2, TOP_NUMBER do
	local remainderList = {};	--Holds the list of remainders
	local endFound = false;	--Holds whether we have found an end to the number (either a cycle or a 0 for remainder)
	local cycleFound = false;	--Holds whether a cycle was detected
	local numerator = 1;	--The numerator that will be divided
	while(not endFound) do
		--Get the remainder after the division
		local remainder = numerator % denominator
		--Check if the remainder is 0
		--If it is, set the flag
		if(remainder == 0) then
			endFound = true;
		--Check if the remainder is in the list
		--If it is in the list, set the appropriate flags
		elseif(remainderList[remainder]) then
			endFound = true;
			cycleFound = true;
		--Else add it to the list
		else
			remainderList[remainder] = true;
		end
		--Multiply the remainder by 10 to continue finding the next remainder
		numerator = remainder * 10;
	end
	--If a cycle was found check the size of the list against the largest cycle
	if(cycleFound) then
		--If it is larger than the largest, set it as the new largest
		if(#remainderList > longestCycle) then
			longestCycle = #remainderList;
			longestNumber = denominator;
		end
	end
end

--End the timer
timer:stop();

--Print the results
io.write("The longest cycle is " .. longestCycle .. " digits long\n");
io.write("It is started with the number " .. longestNumber .. '\n');
io.write("It took " .. timer:getString() .. " to run this algorithm\n");

--[[ Results:
The longest cycle is 982 digits long
It is started with the number 983
It took 28.222 milliseconds to run this algorithm
]]
