--ProjectEuler/lua/Problem30.lua
-- Created: 10-28-19
--Modified: 06-19-20
--Find the sum of all the numbers that can be written as the sum of the fifth powers of their digits.
--All of my requires, unless otherwise listed, can be found at https://bitbucket.org/Mattrixwv/luaClasses
--[[
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]


require "Stopwatch"


--Returns a table with the individual digits of the number passed to it
local function getDigits(num)
	local listOfDigits = {};	--This table holds the individual digits of num
	--The easiest way to get the individual digits of a number is by converting it to a string
	local digits = tostring(num);
	--Start with the first digit, convert it to an integer, store it in the table, and move to the next digit
	for cnt = 1, #digits do
		table.insert(listOfDigits, tonumber(string.sub(digits, cnt, cnt)));
	end
	--Return the table of digits
	return listOfDigits;
end

--Gets the sum of a table of numbers
local function getSumOfTable(ary)
	local sum = 0;	--Start the sum at 0 so you can add to it
	--Add every number in the table to the sum
	for cnt = 1, #ary do
		sum = sum + ary[cnt];
	end
	--Return the sum
	return sum;
end


--Setup the variables
local timer = Stopwatch:create();
local TOP_NUM = 1000000;	--This is the largest number that will be checked
local BOTTOM_NUM = 2;	--Starts with 2 because 0 and 1 don't count
local POWER_RAISED = 5;	--This is the power that the digits are raised to
local sumOfFifthNumbers = {};	--This is a table of the numbers that are the sum of the fifth power of their digits
local sum = 0;	--This is the sum of the sumOfFifthNumbers array

--Start the timer
timer:start();

--Start with the lowest number and increment until you reach the largest number
for currentNum = BOTTOM_NUM, TOP_NUM do
	--Get the digits of the number
	local digits = getDigits(currentNum);
	--Get the sum of the powers
	local sumOfPowers = 0;
	for cnt = 1, #digits do
		sumOfPowers = sumOfPowers + math.pow(digits[cnt], POWER_RAISED);
	end
	--Check if the sum of the powers is the same as the number
	--If it is add it to the list, otherwise continue to the next number
	if(sumOfPowers == currentNum) then
		table.insert(sumOfFifthNumbers, currentNum);
	end
end

sum = getSumOfTable(sumOfFifthNumbers);

--Stop the timer
timer:stop();

--Print the results
io.write("The sum of all the numbers that can be written as the sum of the fifth powers of their digits is " .. sum .. '\n');
io.write("It took " .. timer:getString() .. " to run this algorithm\n");


--[[ Results:
The sum of all the numbers that can be written as the sum of the fifth powers of their digits is 443839
It took 2.706 seconds to run this algorithm
]]
