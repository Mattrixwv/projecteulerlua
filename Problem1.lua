--ProjectEuler/lua/Problem1.lua
--Matthew Ellison
-- Created: 02-01-19
--Modified: 10-26-20
--What is the sum of all the multiples of 3 or 5 that are less than 1000
--All of my requires, unless otherwise listed, can be found at https://bitbucket.org/Mattrixwv/luaClasses
--[[
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]


require "Stopwatch"


local TOP_NUMBER = 999;	--This is the largest number that you are going to check
local sumOfMultiples = 0;


local timer = Stopwatch:create();
timer:start();

--Gets the sum of the progression of the multiple
local function sumOfProgression(multiple)
	local numTerms = TOP_NUMBER // multiple;	--This gets the number of multiples of a particular number that is < MAX_NUMBER
	--The sum of progression formula is (n / 2)(a + l). n = number of terms, a = multiple, l = last term
	return ((numTerms / 2) * (multiple + (numTerms * multiple)));
end

--Get the sum of the progressions of 3 and 5 and remove the sum of progressions of the overlap
sumOfMultiples = math.floor(sumOfProgression(3)) + math.floor(sumOfProgression(5)) - math.floor(sumOfProgression(3 * 5));

timer:stop()

--Print the results
print("The sum of all numbers < 1000 is " .. sumOfMultiples);
print("It took " .. timer:getMicroseconds() .. " microseconds to run this algorithm");


--[[Results:
The sum of all numbers < 1000 is 233168
It took 2.0 microseconds to run this algorithm
]]
