--ProjectEuler/lua/Problem24.lua
--Matthew Ellison
-- Created: 03-24-19
--Modified: 06-19-20
--What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
--All of my requires, unless otherwise listed, can be found at https://bitbucket.org/Mattrixwv/luaClasses
--[[
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]


require "Stopwatch"
require "Algorithms"

NEEDED_PERM = 1000000;	--The number of the permutation that you need


--Setup the variables
local timer = Stopwatch:create();
local nums = "0123456789";

--Start the timer
timer:start();

--Get all permutations of the string
local permutations = getPermutations(nums);

--Stop the timer
timer:stop();

--Print the results
io.write("The 1 millionth permutation is " .. permutations[NEEDED_PERM] .. '\n');
io.write("It took " .. timer:getSeconds() .. " seconds to run this algorithm\n");

--[[ Results:
The 1 millionth permutation is 2783915460
It took 50.175 seconds to run this algorithm
]]
