--ProjectEuler/lua/Problem11.lua
--Matthew Ellison
-- Created: 02-06-19
--Modified: 06-19-20
--What is the greatest product of four adjacent numbers in the same direction (up, down, left, right, or diagonally) in the 20×20 grid?
--[[
08 02 22 97 38 15 00 40 00 75 04 05 07 78 52 12 50 77 91 08
49 49 99 40 17 81 18 57 60 87 17 40 98 43 69 48 04 56 62 00
81 49 31 73 55 79 14 29 93 71 40 67 53 88 30 03 49 13 36 65
52 70 95 23 04 60 11 42 69 24 68 56 01 32 56 71 37 02 36 91
22 31 16 71 51 67 63 89 41 92 36 54 22 40 40 28 66 33 13 80
24 47 32 60 99 03 45 02 44 75 33 53 78 36 84 20 35 17 12 50
32 98 81 28 64 23 67 10 26 38 40 67 59 54 70 66 18 38 64 70
67 26 20 68 02 62 12 20 95 63 94 39 63 08 40 91 66 49 94 21
24 55 58 05 66 73 99 26 97 17 78 78 96 83 14 88 34 89 63 72
21 36 23 09 75 00 76 44 20 45 35 14 00 61 33 97 34 31 33 95
78 17 53 28 22 75 31 67 15 94 03 80 04 62 16 14 09 53 56 92
16 39 05 42 96 35 31 47 55 58 88 24 00 17 54 24 36 29 85 57
86 56 00 48 35 71 89 07 05 44 44 37 44 60 21 58 51 54 17 58
19 80 81 68 05 94 47 69 28 73 92 13 86 52 17 77 04 89 55 40
04 52 08 83 97 35 99 16 07 97 57 32 16 26 26 79 33 27 98 66
88 36 68 87 57 62 20 72 03 46 33 67 46 55 12 32 63 93 53 69
04 42 16 73 38 25 39 11 24 94 72 18 08 46 29 32 40 62 76 36
20 69 36 41 72 30 23 88 34 62 99 69 82 67 59 85 74 04 36 16
20 73 35 29 78 31 90 01 74 31 49 71 48 86 81 16 23 57 05 54
01 70 54 71 83 51 54 69 16 92 33 48 61 43 52 01 89 19 67 48	
]]
--All of my requires, unless otherwise listed, can be found at https://bitbucket.org/Mattrixwv/luaClasses
--[[
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]


require "Stopwatch"


local timer = Stopwatch:create();
timer:start();

--Setup the grid of numbers
local GRID = {};
GRID[1] = {8, 2, 22, 97, 38, 15, 00, 40, 00, 75, 4, 5, 7, 78, 52, 12, 50, 77, 91, 8};
GRID[2] = {49, 49, 99, 40, 17, 81, 18, 57, 60, 87, 17, 40, 98, 43, 69, 48, 4, 56, 62, 00};
GRID[3] = {81, 49, 31, 73, 55, 79, 14, 29, 93, 71, 40, 67, 53, 88, 30, 3, 49, 13, 36, 65};
GRID[4] = {52, 70, 95, 23, 4, 60, 11, 42, 69, 24, 68, 56, 1, 32, 56, 71, 37, 2, 36, 91};
GRID[5] = {22, 31, 16, 71, 51, 67, 63, 89, 41, 92, 36, 54, 22, 40, 40, 28, 66, 33, 13, 80};
GRID[6] = {24, 47, 32, 60, 99, 3, 45, 2, 44, 75, 33, 53, 78, 36, 84, 20, 35, 17, 12, 50};
GRID[7] = {32, 98, 81, 28, 64, 23, 67, 10, 26, 38, 40, 67, 59, 54, 70, 66, 18, 38, 64, 70};
GRID[8] = {67, 26, 20, 68, 2, 62, 12, 20, 95, 63, 94, 39, 63, 8, 40, 91, 66, 49, 94, 21};
GRID[9] = {24, 55, 58, 5, 66, 73, 99, 26, 97, 17, 78, 78, 96, 83, 14, 88, 34, 89, 63, 72};
GRID[10] = {21, 36, 23, 9, 75, 00, 76, 44, 20, 45, 35, 14, 00, 61, 33, 97, 34, 31, 33, 95};
GRID[11] = {78, 17, 53, 28, 22, 75, 31, 67, 15, 94, 3, 80, 4, 62, 16, 14, 9, 53, 56, 92};
GRID[12] = {16, 39, 5, 42, 96, 35, 31, 47, 55, 58, 88, 24, 00, 17, 54, 24, 36, 29, 85, 57};
GRID[13] = {86, 56, 00, 48, 35, 71, 89, 7, 5, 44, 44, 37, 44, 60, 21, 58, 51, 54, 17, 58};
GRID[14] = {19, 80, 81, 68, 5, 94, 47, 69, 28, 73, 92, 13, 86, 52, 17, 77, 4, 89, 55, 40};
GRID[15] = {4, 52, 8, 83, 97, 35, 99, 16, 7, 97, 57, 32, 16, 26, 26, 79, 33, 27, 98, 66};
GRID[16] = {88, 36, 68, 87, 57, 62, 20, 72, 3, 46, 33, 67, 46, 55, 12, 32, 63, 93, 53, 69};
GRID[17] = {4, 42, 16, 73, 38, 25, 39, 11, 24, 94, 72, 18, 8, 46, 29, 32, 40, 62, 76, 36};
GRID[18] = {20, 69, 36, 41, 72, 30, 23, 88, 34, 62, 99, 69, 82, 67, 59, 85, 74, 4, 36, 16};
GRID[19] = {20, 73, 35, 29, 78, 31, 90, 1, 74, 31, 49, 71, 48, 86, 81, 16, 23, 57, 5, 54};
GRID[20] = {1, 70, 54, 71, 83, 51, 54, 69, 16, 92, 33, 48, 61, 43, 52, 1, 89, 19, 67, 48};

--This function returns the product of all elements in a table
local function getTableProduct(numbers)
	local product = 1;	--Start with 1 because you are working with multiplication
	--Loop through ever element in a table and multiply them all together
	for location=1,#numbers do
		product = product * numbers[location];
	end
	--Return the product of all elements
	return product;
end

local greatestNumbers = {0, 0, 0, 0};	--Holds the numbers that give the greatest product
local currentNumbers = {0, 0, 0, 0};	--Holds the numbers that you are currently working with
--Loop through ever location in the grid
for row=1,#GRID do
	for col=1,#GRID[row] do
		--Setup variables for know what direction you can move
		local moveLeft = false;
		local moveRight = false;
		local moveDown = false;

		--Check which directions you will be able to move
		if((col - 3) >= 1) then
			moveLeft = true;
		end
		if((col + 3) <= #GRID[row]) then
			moveRight = true;
		end
		if((row + 3) <= #GRID) then
			moveDown = true;
		end

		--With these movements check for the greatest product of 4 adjacent numbers
		--Move Right
		if(moveRight) then
			currentNumbers[1] = GRID[row][col];
			currentNumbers[2] = GRID[row][col + 1];
			currentNumbers[3] = GRID[row][col + 2];
			currentNumbers[4] = GRID[row][col + 3];
			--Check if the current set is greater than the maximum
			if(getTableProduct(currentNumbers) > getTableProduct(greatestNumbers)) then
				greatestNumbers[1] = currentNumbers[1];
				greatestNumbers[2] = currentNumbers[2];
				greatestNumbers[3] = currentNumbers[3];
				greatestNumbers[4] = currentNumbers[4];
			end
		end

		--Move Down
		if(moveDown) then
			currentNumbers[1] = GRID[row][col];
			currentNumbers[2] = GRID[row + 1][col];
			currentNumbers[3] = GRID[row + 2][col];
			currentNumbers[4] = GRID[row + 3][col];
			--Check if the current set is greater than the maximum
			if(getTableProduct(currentNumbers) > getTableProduct(greatestNumbers)) then
				greatestNumbers[1] = currentNumbers[1];
				greatestNumbers[2] = currentNumbers[2];
				greatestNumbers[3] = currentNumbers[3];
				greatestNumbers[4] = currentNumbers[4];
			end
		end

		--Move Down & Left
		if(moveDown and moveLeft) then
			currentNumbers[1] = GRID[row][col];
			currentNumbers[2] = GRID[row + 1][col - 1];
			currentNumbers[3] = GRID[row + 2][col - 2];
			currentNumbers[4] = GRID[row + 3][col - 3];
			
			--Check if the current set is greater than the maximum
			if(getTableProduct(currentNumbers) > getTableProduct(greatestNumbers)) then
				greatestNumbers[1] = currentNumbers[1];
				greatestNumbers[2] = currentNumbers[2];
				greatestNumbers[3] = currentNumbers[3];
				greatestNumbers[4] = currentNumbers[4];
			end
		end

		--Move Down & Right
		if(moveDown and moveRight) then
			currentNumbers[1] = GRID[row][col];
			currentNumbers[2] = GRID[row + 1][col + 1];
			currentNumbers[3] = GRID[row + 2][col + 2];
			currentNumbers[4] = GRID[row + 3][col + 3];
			--Check if the current set is greater than the maximum
			if(getTableProduct(currentNumbers) > getTableProduct(greatestNumbers)) then
				greatestNumbers[1] = currentNumbers[1];
				greatestNumbers[2] = currentNumbers[2];
				greatestNumbers[3] = currentNumbers[3];
				greatestNumbers[4] = currentNumbers[4];
			end
		end
	end
end

timer:stop();

--Print the results
print("The greatest product of 4 numbers in a line is " .. getTableProduct(greatestNumbers));
print("The numbers are " .. greatestNumbers[1] .. ' ' .. greatestNumbers[2] .. ' ' .. greatestNumbers[3] .. ' ' .. greatestNumbers[4]);
print("It took " .. timer:getMilliseconds() .. " milliseconds to run this algorithm");

--[[Results:
The greatest product of 4 numbers in a line is 70600674
The numbers are 89 94 97 87
It took 0.701 milliseconds to run this algorithm
]]
