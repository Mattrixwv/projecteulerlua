--ProjectEuler/ProjectEulerLua/Problem38.lua
--Matthew Ellison
-- Created: 10-11-21
--Modified: 10-11-21
--What is the largest 1-9 pandigital number that can be formed as the concatenated product of an integer with 1, 2, ... n where n > 1
--All of my requires, unless otherwise listed, can be found at https://bitbucket.org/Mattrixwv/luaClasses
--[[
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]


require "Stopwatch"
require "Algorithms"


--Setup the variables
local timer = Stopwatch:create();
local HIGHEST_POSSIBLE_NUM = 9999;	--The highest number that needs to be checked for a 1-9 pandigital
local largestNum = 0;	--The number passed to the executeFormula function that returns the largest pandigital
local pandigital = 0;	--The largest pandigital number found

--Functions
--Take the number ad add its multiples to a string to return
local function executeFormula(num)
	--Turn the current number into a string
	local numStr = tostring(num);
	numStr = numStr .. tostring(num * 2);
	--Multiply the number and append the product to the string until you have one long enough
	local cnt = 3;
	while(#numStr < 9) do
		numStr = numStr .. tostring(num * cnt);
		cnt = cnt + 1;
	end

	return numStr;
end


--Start the timer
timer:start();


--Loop from 1 -> HIGHEST_POSSIBLE_NUM checking for pandigitals
for cnt = 1, HIGHEST_POSSIBLE_NUM do
	--Get the string from the formula
	local numStr = executeFormula(cnt);
	local panNum = tonumber(numStr);
	--If the number is pandigital save it as the highest number
	if(isPandigital(numStr) and (panNum > pandigital)) then
		largestNum = cnt;
		pandigital = panNum;
	end
end


--Stop the timer
timer:stop();

--Print the results
io.write("The largest appened product pandigital is " .. pandigital .. "\n");
io.write("It took " .. timer:getString() .. " to run this algorithm\n");


--[[ Results:
The largest appened product pandigital is 932718654
It took 14.555 milliseconds to run this algorithm
]]
