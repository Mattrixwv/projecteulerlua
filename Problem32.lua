--ProjectEuler/ProjectEulerLua/Problem32.lua
-- Created: 07-28-20
--Modified: 07-28-20
--Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.
--All of my requires, unless otherwise listed, can be found at https://bitbucket.org/Mattrixwv/luaClasses
--[[
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]


require "Stopwatch"


--Setup the variables
local timer = Stopwatch:create();
local topMultiplicand = 99;	--The largest multiplicand to check
local topMultiplier = 4999;	--The largest multiplier to check
local listOfProducts = {};	--The list of unique products that are 1-9 pandigital
local sumOfPandigitals = 0;	--The sum of the products of the pandigital numbers

--Functions
local function getProduct(currentSet)
	return currentSet.multiplicand * currentSet.multiplier;
end
local function getNumString(currentSet)
	return tostring(currentSet.multiplicand)..tostring(currentSet.multiplier)..tostring(getProduct(currentSet));
end
local function isPandigital(currentSet)
	--Get the number out of the object and put them into a string
	local numberString = getNumString(currentSet);
	--Make sure the string is the correct length
	if(#numberString ~= 9) then
		return false;
	end
	--Make sure there is exactly one of this number contained in the string
	for panNumber=1,9 do
		--Make sure there is exactly one of this number contained in the string
		local _, count = string.gsub(numberString, tostring(panNumber), tostring(panNumber));
		if(count ~= 1) then
			return false;
		end
	end
	--If all numbers wer found in the string return true
	return true;
end
--Returns true if the product of the currentSet is in the table
local function productInTable(currentSet)
	for set=1,#listOfProducts do
		if(getProduct(listOfProducts[set]) == getProduct(currentSet)) then
			return true;
		end
	end
	return false;
end

--Start the timer
timer:start();

--Create the multiplicand and start working your way up
for multiplicand=1,topMultiplicand do
	--Run through all possible multipliers
	for multiplier=multiplicand,topMultiplier do
		local currentProductSet = {multiplicand = multiplicand, multiplier = multiplier};
		--If the product is too long move on to the next possible number
		if(#getNumString(currentProductSet) > 9) then
			break;
		end
		--If the current number is a pandigital that doesn't already exist in the list add it
		if(isPandigital(currentProductSet)) then
			if(not productInTable(currentProductSet)) then
				table.insert(listOfProducts, currentProductSet);
			end
		end
	end
end

--Get the sum of the products of the pandigitals
for prod=1,#listOfProducts do
	sumOfPandigitals = sumOfPandigitals + getProduct(listOfProducts[prod]);
end

--Stop the timer
timer:stop();

--Print the results
io.write("There are " .. #listOfProducts .. " unique 1-9 pandigitals\n");
io.write("The sum of the products of these pandigitals is " .. sumOfPandigitals .. '\n');
io.write("It took " .. timer:getString() .. " to run this algorithm\n");

--[[ Results:
There are 7 unique 1-9 pandigitals
The sum of the products of these pandigitals is 45228
It took 100.000 milliseconds to run this algorithm
]]
